<!DOCTYPE html>

<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
		
		<title>JumpIn Travel</title>
		
		<!-- Bootstrap Core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		
		<!-- Customizable CSS -->
		<link href="assets/css/main.css" rel="stylesheet">
		<link href="assets/css/blue.css" rel="stylesheet" title="Color">
		<link href="assets/css/responsive.css" rel="stylesheet">
		<link href="assets/css/owl.carousel.css" rel="stylesheet">
		<link href="assets/css/owl.transitions.css" rel="stylesheet">
		<link href="assets/css/animate.min.css" rel="stylesheet">
		
		<!-- Fonts -->
		<link href="http://fonts.googleapis.com/css?family=Lato:400,900,300,700" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic" rel="stylesheet">
		
		<!-- Icons/Glyphs -->
		<link href="assets/fonts/fontello.css" rel="stylesheet">
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/images/favicon.png">
	</head>
	
	<body>
		
		<!-- HEADER -->
		
		<header>
			<div class="navbar">
				
				<div class="navbar-header">
					<div class="container">
						
						<ul class="info pull-left">
						</ul><!-- /.info -->
						
						<ul class="social pull-right">
							<li>
								<a href="#"><i class="icon-s-facebook"></i></a>
							</li>
							<li><a href="#"><i class="icon-s-gplus"></i></a></li>
							<li><a href="#"><i class="icon-s-twitter"></i></a></li>
							<li><a href="๒"><i class="icon-s-pinterest"></i></a></li>
							
						</ul><!-- /.social -->
						
						<!-- LOGO MOBILE -->
						
						<a class="navbar-brand" href="index.html">JUMPIN Logo</a>
						
						<!-- LOGO MOBILE : END -->
						
						<a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu-1'></i></a>
						
					</div><!-- /.container -->
				</div><!-- /.navbar-header -->
				
				<div class="yamm">
					<div class="navbar-collapse collapse" >
						<div class="container">
							
							<!-- LOGO -->
							
							<a class="navbar-brand" href="index.html">
								<img src="assets/images/logo.png" alt="">
							</a>
							
							<!-- LOGO : END -->
							
							
							<!-- MAIN NAVIGATION -->
							
							<ul class="nav navbar-nav" >
								
								<li class="dropdown">
									<a href="#" class="js-activated">Home</a>
								</li>
								
								<li class="dropdown">
									<a href="portfolio2.html#" class="dropdown-toggle js-activated">Tip</a>
									<ul class="dropdown-menu">
										<li><a href="#">Tip Category</a></li>
										<li><a href="#">Tip Category</a></li>
										<li><a href="#">Tip Category</a></li>
										<li><a href="#">Tip Category</a></li>
									</ul><!-- /.dropdown-menu -->
								</li><!-- /.dropdown -->
								
								<li class="dropdown">
									<a href="#" class="dropdown-toggle js-activated">traveler</a>
									
									
								</li>
								
								<li class="dropdown">
									<a href="#" class="dropdown-toggle js-activated">Location</a>
									
									<ul class="dropdown-menu">
										<li><a href="#">Location Area</a></li>
									</ul><!-- /.dropdown-menu -->
								</li><!-- /.dropdown -->
								
								
								
								<li class="dropdown">
									<a href="#" class="dropdown-toggle js-activated">Contact</a>
								</li><!-- /.dropdown -->
								
								<li class="dropdown pull-right searchbox">
									<a href="#" class="dropdown-toggle js-activated"><i class="icon-search"></i></a>
											  
									<div class="dropdown-menu">
										<form id="search" class="navbar-form search" role="search">
											<input type="search" class="form-control" placeholder="Type to search">
											<button type="submit" class="btn btn-default btn-submit icon-right-open"></button>
										</form>
									</div><!-- /.dropdown-menu -->
								</li><!-- /.searchbox -->
								
							</ul><!-- /.nav -->
							
							<!-- MAIN NAVIGATION : END -->
							
						</div><!-- /.container -->
					</div><!-- /.navbar-collapse -->
				</div><!-- /.yamm -->
			</div><!-- /.navbar -->
		</header>
		
		<!-- HEADER : END -->
		