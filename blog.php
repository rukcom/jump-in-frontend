<? include('header.php');?>
		<!--  MAIN  -->
		
		<main>
			
			<!--  SECTION – Hilight POSTS  -->
			<section id="portfolio">
				
			
				<div class="portfolio">
					
					
					
					<ul class="items fullscreen">
						
						<li class="item thumb identity">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Signwall</h4>
											<p>Identity</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work16.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Handmade Wood Gifts</h4>
											<p>Identity</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work17.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb interactive">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Elena Website</h4>
											<p>Interactive</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work22.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity print">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>GYHL Stationery</h4>
											<p>Identity/Print</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work23.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb print">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Electric Dance Book Cover</h4>
											<p>Print</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work20.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb interactive">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Appscreen Dashboard</h4>
											<p>Interactive</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work01.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb print">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Free Skate Book Cover</h4>
											<p>Print</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work24.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb print">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Modern CD Case</h4>
											<p>Print</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work18.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Designer Brand</h4>
											<p>Identity</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work19.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb print">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Tri Fold Brochure</h4>
											<p>Print</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work10.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb photography">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Waterfalls</h4>
											<p>Photography</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/photograph03.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb print">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Magazine Layout</h4>
											<p>Print</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work25.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Simpli Nota</h4>
											<p>Identity</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work04.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Brand Stationery</h4>
											<p>Identity</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work08a.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Grand Motel</h4>
											<p>Identity</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work02.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity print">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Brand Stationery</h4>
											<p>Identity/Print</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work21.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Embroidered</h4>
											<p>Identity</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work05a.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Astor & Yancy</h4>
											<p>Identity</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work09.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb identity">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Martin Johnson</h4>
											<p>Identity</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work06.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						<li class="item thumb print">
							<a href="#">
								<figure>
									<figcaption class="text-overlay">
										<div class="info">
											<h4>Vinyl Records</h4>
											<p>Print</p>
										</div><!-- /.info -->
									</figcaption>
									<img src="assets/images/art/work07.jpg" alt="">
								</figure>
							</a>
						</li><!-- /.item -->
						
						
					</ul><!-- /.items -->
					
				</div><!-- /.portfolio -->
			</section>
			<!--  SECTION – Hilight POSTS : END  -->
			
			
			<!--  SECTION – BLOG  -->
			
			<section id="blog" class="light-bg">
				
				<div class="container inner-top-sm inner-bottom classic-blog">
				<div class="panel text-center row text-center">

					<form action="" class="forms form-filter">
					
							<div class="col-xs-6 col-sm-3"><input type="text" placeholder="Tip Category" class="form-control"></div>
							<div class="col-xs-6 col-sm-3"><input type="text" placeholder="Tip Location"></div>
							<div class="col-xs-6 col-sm-3"><input type="text" placeholder="Tip "></div>
							<div class="col-xs-6 col-sm-3"><button type="submit" class="btn btn-default"><i class="icon-search-1"></i></button></div>
						
					</form>
				</div>

					<h2>Category List</h2>
					
					<div class="row">
						
						<div class="col-md-9 inner-right-sm">
							
							<div class="posts sidemeta">
								
								<div class="post format-gallery">
									
									<div class="date-wrapper">
										<div class="date">
											<span class="day">23</span>
											<span class="month">May</span>
										</div><!-- /.date -->
									</div><!-- /.date-wrapper -->
									
									<div class="format-wrapper">
										<a href="#" data-filter=".format-gallery"><i class="icon-picture"></i></a>
									</div><!-- /.format-wrapper -->
									
									<div class="post-content">
										
										<div id="owl-work" class="owl-carousel owl-inner-pagination owl-inner-nav post-media">
											
											<div class="item">
												<figure>
													<img src="assets/images/art/work06-lg.jpg" alt="">
												</figure>
											</div><!-- /.item -->
											
											<div class="item">
												<figure>
													<img src="assets/images/art/work02-lg.jpg" alt="">
												</figure>
											</div><!-- /.item -->
											
											<div class="item">
												<figure>
													<img src="assets/images/art/work08a-lg.jpg" alt="">
												</figure>
											</div><!-- /.item -->
											
											<div class="item">
												<figure>
													<img src="assets/images/art/work09-lg.jpg" alt="">
												</figure>
											</div><!-- /.item -->
											
											<div class="item">
												<figure>
													<img src="assets/images/art/work16-lg.jpg" alt="">
												</figure>
											</div><!-- /.item -->
											
										</div><!-- /.owl-carousel -->
										
										<h2 class="post-title">
											<a href="#">Learn how to successfully design brand identities</a>
										</h2>
										
										<ul class="meta">
											<li class="categories"><a href="#">Identity</a>, <a href="#">Graphic Design</a></li>
											<li class="comments"><a href="#">24</a></li>
											<li class="likes"><a href="#">73</a></li>
										</ul><!-- /.meta -->
										
										<p>Magnis modipsae que lib voloratati andigen daepeditem quiate ut repore autem labor. Laceaque quiae sitiorem rest non restibusaes es tumquam core posae volor remped modis volor. Doloreiur qui commolu oreprerum.</p>
										
										<a href="#" class="btn">Read more</a>
										
									</div><!-- /.post-content --> 
									
								</div><!-- /.post -->
								
								<div class="post format-quote">
									
									<div class="date-wrapper">
										<div class="date">
											<span class="day">21</span>
											<span class="month">May</span>
										</div><!-- /.date -->
									</div><!-- /.date-wrapper -->
									
									<div class="format-wrapper">
										<a href="#" data-filter=".format-quote"><i class="icon-quote"></i></a>
									</div><!-- /.format-wrapper -->
									
									<div class="post-content">
										<blockquote>
											<p>Been putting off creating my portfolio for several years, then I found REEN and about 30 minutes later ... I have an awesome looking portfolio. Totally love it – keep up the good work!</p>
											<footer><cite>Bill Jobs</cite></footer>
										</blockquote>
									</div><!-- /.post-content --> 
									
								</div><!-- /.post -->
								
								<div class="post format-standard">
									
									<div class="date-wrapper">
										<div class="date">
											<span class="day">14</span>
											<span class="month">May</span>
										</div><!-- /.date -->
									</div><!-- /.date-wrapper -->
									
									<div class="format-wrapper">
										<a href="#" data-filter=".format-standard"><i class="icon-edit"></i></a>
									</div><!-- /.format-wrapper -->
									
									<div class="post-content">
										
										<h2 class="post-title">
											<a href="#">The ultimate guide for designers to start freelancing</a>
										</h2>
										
										<ul class="meta">
											<li class="categories"><a href="#">Business</a></li>
											<li class="comments"><a href="#">14</a></li>
											<li class="likes"><a href="#">30</a></li>
										</ul><!-- /.meta -->
										
										<p>Magnis modipsae que lib voloratati andigen daepeditem quiate ut repore autem labor. Laceaque quiae sitiorem rest non restibusaes es tumquam core posae volor remped modis volor. Doloreiur qui commolu oreprerum.</p>
										
										<a href="#" class="btn">Read more</a>
										
									</div><!-- /.post-content --> 
									
								</div><!-- /.post -->
								
								<div class="post format-image">
									
									<div class="date-wrapper">
										<div class="date">
											<span class="day">10</span>
											<span class="month">May</span>
										</div><!-- /.date -->
									</div><!-- /.date-wrapper -->
									
									<div class="format-wrapper">
										<a href="#" data-filter=".format-image"><i class="icon-picture-1"></i></a>
									</div><!-- /.format-wrapper -->
									
									<div class="post-content">
										
										<figure class="icon-overlay icn-link post-media">
											<a href="#"><img src="assets/images/art/photograph04-lg.jpg" alt=""></a>
										</figure><!-- /.post-media -->
										
										<h2 class="post-title">
											<a href="#">The girl and the summerlight</a>
										</h2>
										
										<ul class="meta">
											<li class="categories"><a href="#">Photography</a></li>
											<li class="comments"><a href="#">63</a></li>
											<li class="likes"><a href="#">114</a></li>
										</ul><!-- /.meta -->
										
										<p>Magnis modipsae que lib voloratati andigen daepeditem quiate ut repore autem labor. Laceaque quiae sitiorem rest non restibusaes es tumquam core posae volor remped modis volor. Doloreiur qui commolu oreprerum.</p>
										
										<a href="#" class="btn">Read more</a>
										
									</div><!-- /.post-content --> 
									
								</div><!-- /.post -->
								
								<div class="post format-video">
									
									<div class="date-wrapper">
										<div class="date">
											<span class="day">07</span>
											<span class="month">May</span>
										</div><!-- /.date -->
									</div><!-- /.date-wrapper -->
									
									<div class="format-wrapper">
										<a href="#" data-filter=".format-video"><i class="icon-video-1"></i></a>
									</div><!-- /.format-wrapper -->
									
									<div class="post-content">
										
										<div class="video-container post-media">
											<iframe src="http://player.vimeo.com/video/65468064?title=0&byline=0&color=1abb9c" width="750" height="422" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
										</div><!-- /.post-media -->
										
										<h2 class="post-title">
											<a href="#">Pure geometry in motion</a>
										</h2>
										
										<ul class="meta">
											<li class="categories"><a href="#">Motion&nbsp;Graphics</a></li>
											<li class="comments"><a href="#">25</a></li>
											<li class="likes"><a href="#">84</a></li>
										</ul><!-- /.meta -->
										
										<p>Magnis modipsae que lib voloratati andigen daepeditem quiate ut repore autem labor. Laceaque quiae sitiorem rest non restibusaes es tumquam core posae volor remped modis volor. Doloreiur qui commolu oreprerum.</p>
										
										<a href="#" class="btn">Read more</a>
										
									</div><!-- /.post-content --> 
									
								</div><!-- /.post -->
								
								<div class="post format-audio">
									
									<div class="date-wrapper">
										<div class="date">
											<span class="day">05</span>
											<span class="month">May</span>
										</div><!-- /.date -->
									</div><!-- /.date-wrapper -->
									
									<div class="format-wrapper">
										<a href="#" data-filter=".format-audio"><i class="icon-music-1"></i></a>
									</div><!-- /.format-wrapper -->
									
									<div class="post-content">
										
										<div class="post-media">
											<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/14132147&color=1abb9c&auto_play=false&hide_related=false&show_artwork=true"></iframe>
										</div><!-- /.post-media -->
										
										<h2 class="post-title">
											<a href="#">Music and design workflow</a>
										</h2>
										
										<ul class="meta">
											<li class="categories"><a href="#">Audio</a></li>
											<li class="comments"><a href="#">14</a></li>
											<li class="likes"><a href="#">48</a></li>
										</ul><!-- /.meta -->
										
										<p>Magnis modipsae que lib voloratati andigen daepeditem quiate ut repore autem labor. Laceaque quiae sitiorem rest non restibusaes es tumquam core posae volor remped modis volor. Doloreiur qui commolu oreprerum.</p>
										
										<a href="#" class="btn">Read more</a>
										
									</div><!-- /.post-content --> 
									
								</div><!-- /.post -->
								
								<div class="post format-link">
									
									<div class="date-wrapper">
										<div class="date">
											<span class="day">01</span>
											<span class="month">May</span>
										</div><!-- /.date -->
									</div><!-- /.date-wrapper -->
									
									<div class="format-wrapper">
										<a href="#" data-filter=".format-link"><i class="icon-popup"></i></a>
									</div><!-- /.format-wrapper -->
									
									<div class="post-content">
										<h2 class="post-title">
											<a href="http://www.wrapbootstrap.com" target="_blank">Super-Awesome Bootstrap Website Templates</a>
										</h2>
										<a href="http://www.wrapbootstrap.com" target="_blank">www.wrapbootstrap.com</a>
									</div><!-- /.post-content --> 
									
								</div><!-- /.post --> 
								
							</div><!-- /.posts -->
							
							<ul class="pagination">
								<li><a href="#">Prev</a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">Next</a></li>
							</ul><!-- /.pagination --> 
							
						</div><!-- /.col -->
						
						<aside class="col-md-3">
							<div class="sidebox widget">
								<h4>Search Posts</h4>
								
								<form id="search" class="navbar-form search" role="search">
									<input type="search" class="form-control" placeholder="Type to search">
									<button type="submit" class="btn btn-default btn-submit icon-right-open"></button>
								</form>
							</div><!-- /.widget -->

							<div class="sidebox widget">
								<h4>Relate Posts</h4>
								
								<div class="row thumbs gap-xs">
									
									<div class="col-xs-6 thumb">
										<figure class="icon-overlay icn-link">
											<a href="#"><img src="assets/images/art/work17.jpg" alt=""></a>
										</figure>
									</div><!-- /.thumb -->
									
									<div class="col-xs-6 thumb">
										<figure class="icon-overlay icn-link">
											<a href="#"><img src="assets/images/art/photograph02.jpg" alt=""></a>
										</figure>
									</div><!-- /.thumb -->
									
									<div class="col-xs-6 thumb">
										<figure class="icon-overlay icn-link">
											<a href="#"><img src="assets/images/art/work18.jpg" alt=""></a>
										</figure>
									</div><!-- /.thumb -->
									
									<div class="col-xs-6 thumb">
										<figure class="icon-overlay icn-link">
											<a href="#"><img src="assets/images/art/work21.jpg" alt=""></a>
										</figure>
									</div><!-- /.thumb -->
									
									<div class="col-xs-6 thumb">
										<figure class="icon-overlay icn-link">
											<a href="#"><img src="assets/images/art/work23.jpg" alt=""></a>
										</figure>
									</div><!-- /.thumb -->
									
									<div class="col-xs-6 thumb">
										<figure class="icon-overlay icn-link">
											<a href="#"><img src="assets/images/art/work22.jpg" alt=""></a>
										</figure>
									</div><!-- /.thumb -->
									
								</div><!-- /.row -->
							</div><!-- /.widget -->
							
							
							<div class="sidebox widget">
								<h4>Featured</h4>
								
								<figure>
									
									<div class="icon-overlay icn-link">
										<a href="#"><img src="assets/images/art/work01.jpg" alt=""></a>
									</div><!-- /.icon-overlay -->
									
									<figcaption class="bordered no-top-border">
										<div class="info">
											<h4><a href="#">Appscreen Dashboard</a></h4>
											<p>Interactive</p>
										</div><!-- /.info -->
									</figcaption>
									
								</figure>
							</div><!-- /.widget -->
							
							
							<div class="sidebox widget">
								<h4>Tags</h4>
								
								<div class="tagcloud">
									<a href="#" style="font-size: 12pt;">Beautiful</a> <a href="#" style="font-size: 20pt;">TRAVEL</a> <a href="#" style="font-size: 10pt;">Quality</a> <a href="#" style="font-size: 14pt;">Website</a> <a href="#" style="font-size: 16pt;">Thailand</a> <a href="#" style="font-size: 12pt;">Professional</a> <a href="#" style="font-size: 20pt;">Design</a> <a href="#" style="font-size: 10pt;">MooTa</a> <a href="#" style="font-size: 16pt;">itinerary</a> <a href="#" style="font-size: 10pt;">Ticket</a> <a href="#" style="font-size: 19pt;">Bangkok</a> <a href="#" style="font-size: 9pt;">backPack</a> <a href="#" style="font-size: 14pt;">by Bicycle</a> <a href="#" style="font-size: 9pt;">Styles</a> <a href="#" style="font-size: 13pt;">Destination</a> <a href="#" style="font-size: 9pt;">chill to night</a> <a href="#" style="font-size: 22pt;">Love</a> <a href="#" style="font-size: 10pt;">Digital</a> <a href="#" style="font-size: 18pt;">Awesome</a> <a href="#" style="font-size: 12pt;">Passion</a> <a href="#" style="font-size: 13pt;">Typography</a> <a href="#" style="font-size: 13pt;">Clean</a> <a href="#" style="font-size: 9pt;">Easy to use</a> <a href="#" style="font-size: 20pt;">Buy Ticket</a> <a href="#" style="font-size: 12pt;">Success</a>
								</div><!-- /.tag-cloud -->
							</div><!-- /.widget -->
							
						</aside>
						
					</div><!-- /.row -->
					
				</div><!-- /.container -->
			</section>
			
			<!--  SECTION – BLOG : END  -->
			
		</main>
		
		<!--  MAIN : END  -->
<? include('footer.php');?>		
		
		