
		
		<? include('header.php');?>
		<!-- MAIN -->
		
		<main>
			
			<!-- SECTION – Slide -->
			<section id="portfolio">
				<div id="owl-main" class="owl-carousel height-md owl-inner-nav owl-ui-lg">
					
					<div class="item" style="background-image: url('assets/images/slide/slider05.jpg');">
						<div class="container">
							<div class="caption vertical-bottom text-right">
								
								<h1 class="fadeInDown-1 light-color">Caption top center</h1>
								<p class="fadeInDown-2 medium-color">This caption is vertically aligned top, horizontally centered and it fades in down.</p>
								<div class="fadeInDown-3">
									<a href="#" class="btn btn-large">Button</a>
								</div><!-- /.fadeIn -->
								
							</div><!-- /.caption -->
						</div><!-- /.container -->
					</div><!-- /.item -->
					
					<div class="item img-bg img-bg-soft light-bg" style="background-image: url('assets/images/slide/slider03.jpg');">
						<div class="container">
							<div class="caption vertical-center text-left">
								
								<h1 class="fadeInLeft-1 dark-color">Caption <br>left center</h1>
								<p class="fadeInLeft-2 dark-color">This caption is horizontally aligned left, vertically <br>centered and it fades in left.</p>
								<div class="fadeInLeft-3">
									<a href="#" class="btn btn-large">Button</a>
								</div><!-- /.fadeIn -->
								
							</div><!-- /.caption -->
						</div><!-- /.container -->
					</div><!-- /.item -->
					
					<div class="item" style="background-image: url('assets/images/slide/slider02.jpg');">
						<div class="container">
							<div class="caption vertical-center text-left">
								
								<h1 class="fadeInRight-1 light-color">Caption <br>right center</h1>
								<p class="fadeInRight-2 light-color">This caption is horizontally aligned right, vertically <br>centered and it fades in right.</p>
								<div class="fadeInRight-3">
									<a href="#" class="btn btn-large">Button</a>
								</div><!-- /.fadeIn -->
								
							</div><!-- /.caption -->
						</div><!-- /.container -->
					</div><!-- /.item -->
					
					<div class="item" style="background-image: url('assets/images/slide/slider06.jpg');">
						<div class="container">
							<div class="caption vertical-top text-left">
								
								<h1 class="fadeIn-1 dark-bg light-color"><span>Caption block dark background</span></h1>
								<p class="fadeIn-2 dark-color">This block caption is horizontally aligned left, vertically top and it fades in.</p>
								<div class="fadeIn-3">
									<a href="#" class="btn btn-large">Button</a>
								</div><!-- /.fadeIn -->
								
							</div><!-- /.caption -->
						</div><!-- /.container -->
					</div><!-- /.item -->
					
					<div class="item" style="background-image: url('assets/images/slide/slider04.jpg');">
						<div class="container">
							<div class="caption vertical-top text-right">
								
								<h1 class="fadeInUp-1 light-bg dark-color"><span>Caption Block Light Background</span></h1>
								<p class="fadeInUp-2 light-color">This block caption is horizontally aligned right, vertically bottom and it fades in up.</p>
								<div class="fadeInUp-3">
									<a href="#" class="btn btn-large">Button</a>
								</div><!-- /.fadeIn -->
								
							</div><!-- /.caption -->
						</div><!-- /.container -->
					</div><!-- /.item -->
					
				</div><!-- /.owl-carousel -->

				<!-- CONTENT Evente Biker Design -->
				<div class="container inner-bottom">
					<div class="row">
						<div class="col-sm-12 portfolio">
							
							<ul class="filter text-center">
								<li><a href="portfolio2.html#" data-filter="*" class="active">All</a></li>
								<li><a href="portfolio2.html#" data-filter=".identity">Identity</a></li>
								<li><a href="portfolio2.html#" data-filter=".interactive">Interactive</a></li>
								<li><a href="portfolio2.html#" data-filter=".print">Print</a></li>
								<li><a href="portfolio2.html#" data-filter=".photography">Photography</a></li>
							</ul><!-- /.filter -->
							
							<ul class="items col-3 gap">
							
								<li class="item thumb interactive">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work01.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Appscreen Dashboard</a></h4>
												<p>Interactive</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb identity">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work02.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Grand Motel</a></h4>
												<p>Identity</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb identity">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work16.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Signwall</a></h4>
												<p>Identity</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb print">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work18.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Modern CD Case</a></h4>
												<p>Print</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb identity">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work09.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Astor & Yancy</a></h4>
												<p>Identity</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb photography">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/photograph02.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Daydreaming</a></h4>
												<p>Photography</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb identity">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work08a.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Brand Stationery</a></h4>
												<p>Identity</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb print">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work10.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Tri Fold Brochure</a></h4>
												<p>Print</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb interactive">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work03.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Vintage Bicycles</a></h4>
												<p>Interactive</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb print">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work20.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Electric Dance Book Cover</a></h4>
												<p>Print</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb identity">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work05.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Embroidered</a></h4>
												<p>Identity</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb identity print">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work21.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Brand Stationery</a></h4>
												<p>Identity/Print</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb identity">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work17.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Handmade Wood Gifts</a></h4>
												<p>Identity</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb identity">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work19.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Designer Brand</a></h4>
												<p>Identity</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
							
								<li class="item thumb print">
									<figure>
									
										<div class="icon-overlay icn-link">
											<a href=""><img src="assets/images/art/work07.jpg" alt=""></a>
										</div><!-- /.icon-overlay -->
										
										<figcaption class="bordered no-top-border">
											<div class="info">
												<h4><a href="">Vinyl Records</a></h4>
												<p>Print</p>
											</div><!-- /.info -->
										</figcaption>
										
									</figure>
								</li><!-- /.item -->
								
							</ul><!-- /.items -->
							
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container -->
				<!-- CONTENT Eventbiker Design - END -->
				
			</section>
			
			<!-- SECTION – PORTFOLIO : END -->
			
			
			<!-- SECTION – SHARE -->
			
			<section id="share" class="light-bg">
				<div class="container">
					
					<div class="col-sm-4 reset-padding">
						<a href="portfolio2.html#" class="btn-share-md">
							<p class="name">Facebook</p>
							<i class="icon-s-facebook"></i>
							<p class="counter">xxx</p>
						</a>
					</div><!-- /.col -->
					
					<div class="col-sm-4 reset-padding">
						<a href="portfolio2.html#" class="btn-share-md">
							<p class="name">Twitter</p>
							<i class="icon-s-twitter"></i>
							<p class="counter">xxx</p>
						</a>
					</div><!-- /.col -->
					
					<div class="col-sm-4 reset-padding">
						<a href="portfolio2.html#" class="btn-share-md">
							<p class="name">Google +</p>
							<i class="icon-s-gplus"></i>
							<p class="counter">xxx</p>
						</a>
					</div><!-- /.col -->
					
				</div><!-- /.container -->
			</section>
			
			<!-- SECTION – SHARE : END -->
			
		</main>
		
		<!-- MAIN : END -->
		<? include('footer.php');?>